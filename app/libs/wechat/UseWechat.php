<?php

declare(strict_types=1);

namespace app\libs\wechat;

use EasyWeChat\Pay\Application as PayApplication;
use EasyWeChat\MiniApp\Application as MiniAppApplication;

/**
 * 微信助手类
 * 本类在服务AppService中注册，自动注入App容器
 * 本类基于Easywechat 6.x 版本 https://easywechat.com/6.x/
 * 使用方法：
 * 1. 根据项目情况自己实现内部方法的配置
 * 2. 其他任何位置可以直接通过 依赖注入、 参数绑定、 或 app('use_wechat') 直接拿到本类实例
 * 3. 本类调用getPay()等方法时，只会在首次调用时重新实例化 PayApplication ，之后是可以复用的。不调用方法时不会实例化（避免无意义的读库等操作）
 */
class UseWechat
{
	protected $mini_app;

	/**
	 * 获取小程序实例
	 * @return MiniAppApplication
	 */
	public function getMiniApp(): MiniAppApplication
	{
		if (!$this->mini_app) {
			$this->mini_app = new MiniAppApplication(config('weapp'));
		}
		return $this->mini_app;
	}
}
