<?php


namespace app\command;

use think\console\{
	Command,
	Input,
	Output,
	input\Argument,
	input\Option
};


class Example extends Command
{

	private $name = 'example';

	/**
	 * 配置命令
	 */
	protected function configure()
	{
		parent::configure();

		/**
		 * 指令和参数的说明
		 * 例如: php think example AA BB --opt=ccc -o ccc
		 * Argument - 指令，其中 example AA BB 三个都是指令，按顺序传入，空格分割
		 * Option	- 参数，其中 --opt=ccc 是参数的完整写法，或者简写 -o ccc (前提是该参数配置了$sort_name)
		 */

		// 以下是示例
		$this->setName($this->name)
			->addArgument('table', Argument::REQUIRED, "要创建的表名") //数据表名
			->addArgument('name', Argument::REQUIRED, "模型的中文别名") //模型别名（中文名）
			->addOption('mode', 'm',  Option::VALUE_OPTIONAL, 'Admin端控制器的模块名', 'adminapi') //模块名，默认是adminapi
			// 下面这个 Option::VALUE_IS_ARRAY 还有bug,等待提PR修复，传参方式可以用 -c a -c b 即可
			// ->addOption('create', 'c', Option::VALUE_IS_ARRAY, '要创建的文件，传入数组：controller控制器，mode模型。默认是controller,mode', ['controller', 'mode']) // 传值表示不创建控制器
			->setDescription('根据MySQL的表自动生成模型和Admin端的控制器');
	}

	/**
	 * 执行
	 */
	protected function execute(Input $input, Output $output)
	{
		$arguments = $input->getArguments();
		$options = $input->getOptions();

		// 执行需要的操作
		echo ('这是示例命令行类');
	}
}
