<?php

namespace app\middleware;

use app\model\{
	Admin,
	User
};
use think\Request;

/**
 * 注解鉴权
 * 通过反射获取当前请求的控制器类和方法，并解析注解中的指定规则执行鉴权
 * 本中间件会在$request上挂载一个 AuthComment 对象，它是本类的实例
 * 在控制器中可以通过 request()->middleware('AuthComment') 或者 request->AuthComment 来获取本实例
 */
class AuthComment
{

	/**
	 * 储存要求登陆的模型数据
	 * @var array
	 */
	protected $loginModels = [];

	public function handle($request, \Closure $next)
	{
		// 要求登陆的规则，解析完成后储存到 $this->loginModels 中
		$this->loginModels = [];
		$loginModelRules = $this->getLoginModelRule($request);
		if(!empty($loginModelRules)){
			$pass = false;
			$models = [];
			foreach ($loginModelRules as $loginModel) {
				if ($loginModel == 'Admin') {
					$admin = Admin::getLoginData((string)input('TOKEN'));
					$models[$loginModel] = $admin;
					if($admin) $pass = true;
				}
				if ($loginModel == 'User') {
					$user = User::getLoginData((string)input('TOKEN'));
					$models[$loginModel] = $user;
					if($user)$pass = true;
				}
			}
			if(!$pass){
				return failed('身份无效，请重新登陆', 403);
			}
			$this->loginModels = $models;
		}

		// 挂载本实例到$request
		$request->AuthComment = $this;

		return $next($request);
	}

	/**
	 * 获取
	 *
	 * @param [type] $name
	 * @return void
	 */
	public function getLoginModel($name = null)
	{
		if(is_null($name)){
			return $this->loginModels;
		}
		return $this->loginModels[$name] ?? null;
	}

	/**
	 * 解析模型登陆功能的规则
	 * 例如: @AuthComment loginModels ["modelA","modelB"]
	 * 其中 ["modelA","modelB"] 称为 鉴权模型，它一定是数组
	 * 鉴权模型的含义和优先级规则如下:
	 * 1. 控制器的 function 中的注解会覆盖 class 中的注解
	 * 2. 空数组被认为等同于不设置任何鉴权
	 * 3. 数组中多个模型只要有一个通过，即可通过中间件。其中通过的模型会被挂载到$request上，未通过的会是null （如果想实现异或关系也可以通过注解来规定一个开关从而实现）
	 * 
	 * @param Request $request
	 * @return array
	 */
	protected function getLoginModelRule(Request $request)
	{
		$loginModels = [];
		$className = $this->getControllerClassName($request);
		$class = new \ReflectionClass($className);
		foreach ($this->parseComment($class->getDocComment()) as $line) {
			if ($line[0] != 'AuthComment') continue;
			if ($line[1] == 'loginModels') {
				$loginModels = json_decode($line[2], true);
			}
		}

		$actionName = $request->action();
		if ($class->hasMethod($actionName)) {
			$action = $class->getMethod($actionName);
			foreach ($this->parseComment($action->getDocComment()) as $line) {
				if ($line[0] != 'AuthComment') continue;
				if ($line[1] == 'loginModels') {
					$loginModels = json_decode($line[2], true);
				}
			}
		}
		return $loginModels;
	}

	/**
	 * 通过Request解析当前控制器类名
	 * @param Request $request
	 * @return void
	 */
	protected function getControllerClassName(Request $request)
	{
		// 这是单应用模式分级控制器的情况，其他情况需要额外处理
		return 'app\\controller\\' . str_replace('.', '\\', $request->controller());
	}

	/**
	 * 将comment解析为
	 * @开头 每行一个元素
	 * @param string $commit 注释全文
	 * @return array
	 */
	protected function parseComment(string $comment)
	{
		$line_array = explode(PHP_EOL, $comment);
		$result = [];
		foreach ($line_array as $line) {
			// 过滤无@开头的项
			if (strpos($line, '@') === false) continue;

			// 移除@之前的符号 [/ * @title 标题] ===> [title 标题]
			$res = preg_replace("/.+@/", '', $line);

			// 除去多个空格的情况 [param int id  商品ID] ===> [param int id 商品ID]
			$res = preg_replace("/\s+/", ' ', $res);

			// 按空格分隔 ['param', 'int', 'id', '商品ID'], 
			$result[] = explode(' ', $res);
		}
		return $result;
	}
}
