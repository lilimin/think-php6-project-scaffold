<?php

declare(strict_types=1);

namespace app;

use think\App as ThinkApp;
use think\Exception;

/**
 * 重载 think\App 类
 * 拓展或覆写一些方法
 */
class App extends ThinkApp
{
	public function __construct(string $rootPath = '')
	{
		parent::__construct($rootPath);
	}

	/**
	 * 自定义加载环境变量
	 * App在实例化时会自动调用一次本方法，所以直接在本方法中实现加载多环境配置文件
	 * 支持加载多个环境变量文件，依次覆盖。需要在.env中设置
	 * APP_EXTRA_ENVS = '.env.a,.env.b'
	 * 即可加载.env.a文件，然后加载.env.b文件
	 * @access public
	 * @param string $envName 忽略该参数
	 */
	public function loadEnv(string $envName = ''): void
	{
		// 加载基础环境变量
		$baseEnvName = $this->rootPath . '.env';
		if (is_file($baseEnvName)) $this->env->load($baseEnvName);

		$extraEnvs = $this->env->get('APP_EXTRA_ENVS');
		if ($extraEnvs) {
			$extraEnvs = explode(',', $extraEnvs);
		}
		foreach ($extraEnvs as $fileName) {
			$realPath = $this->rootPath . $fileName;
			if (is_file($realPath)) {
				$this->env->load($realPath);
			} else {
				// throw new Exception('Load ENV Faild! ENV file [' . $realPath . '] is not found, check [APP_EXTRA_ENVS] in .env file.');
			}
		}
	}
}
