<?php

namespace app\validate;

use think\Validate;

use app\model\{
    User,
    GoodType,
    Supplier
};


class BaseValidate extends Validate
{
    // 验证手机号
    protected function checkMobile($value, $rule, $data, $key, $key_desc)
    {
        return preg_match('/^1[3456789]\d{9}$/', $value) ? true : $key_desc . '不符合规则';
    }

    // 验证用户id
    protected function checkUserId($value, $rule, $data, $key, $key_desc)
    {
        return User::find($value) ? true : $key_desc . '不存在';
    }
}
