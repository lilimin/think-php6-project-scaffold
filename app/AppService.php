<?php
declare (strict_types = 1);

namespace app;

use think\Service;
use app\libs\wechat\UseWechat;

/**
 * 应用服务类
 */
class AppService extends Service
{
    public function register()
    {
        // 服务注册

        // 注册微信SDK服务
        $this->app->bind('use_wechat', UseWechat::class);
    }

    public function boot()
    {
        // 服务启动
    }
}
