<?php

namespace app\controller;

use app\BaseController;
use openapi\ApiDoc;
use app\model\{
	Admin,
	Order,
	Goods
};
use think\facade\Queue;
use Carbon\Carbon;
use think\facade\Cache;
use think\facade\Db;
use throttling\ThrottlingFunnel;
use poster\Poster;
use think\exception\ValidateException;

/**
 * 默认控制器
 */
class Index extends BaseController
{
	/**
	 * 根路径
	 */
	public function index()
	{
		// return '正在访问 ThinkPHP 6.0 项目脚手架 - ' . env('app.mode');
		dump(1 & 2);
	}

	/**
	 * 测试请求漏斗节流
	 */
	public function funnelThrottle()
	{
		$ip = request()->ip();
		$thro = new ThrottlingFunnel('funnelThrottle:' . $ip);
		if (!$thro->inc()) {
			return '当前请求次数' . $thro->getCount() . ', 限制条件为最大' . $thro->getConfig('max_count') . '个水滴，每个水滴时长' . $thro->getConfig('out_dely');
		}
		return '请求成功 funnelThrottle:' . $ip;
	}

	/**
	 * 获取接口文档分组数据
	 *
	 */
	public function getApiGroup()
	{
		$scheme = $this->request->scheme();
		return json([
			[
				'name' => '后台管理端',
				'link' => $scheme . '://' . env('app.host') . '/index/getApiDocJson?group=adminapi'
			],
			[
				'name' => '公共接口',
				'link' => $scheme . '://' . env('app.host') . '/index/getApiDocJson?group=commonapi'
			],
		]);
	}

	/**
	 * 获取接口文档数据
	 *
	 * @param string $group 分组名称
	 */
	public function getApiDocJson($group = '')
	{
		if ($group) $group = '/' . $group;
		$oas = (new ApiDoc(__DIR__ . $group))->parseClassToApi();
		return json($oas);
	}

	/**
	 * 测试合成海报
	 */
	public function testPoster()
	{
		$tempDir = app()->getRuntimePath() . 'temp' . DIRECTORY_SEPARATOR . 'tc2302' . DIRECTORY_SEPARATOR;
		if (!is_dir($tempDir)) {
			mkdir($tempDir, 0777, true);
		}

		// 二维码
		// use Endroid\QrCode\Color\Color;
		// use Endroid\QrCode\Encoding\Encoding;
		// use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
		// use Endroid\QrCode\QrCode;
		// use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
		// use Endroid\QrCode\Writer\PngWriter;

		// $qrCode = QrCode::create($shareUrl)
		// 	->setEncoding(new Encoding('UTF-8'))
		// 	->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
		// 	->setSize(300)
		// 	->setMargin(10)
		// 	->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
		// 	->setForegroundColor(new Color(0, 0, 0))
		// 	->setBackgroundColor(new Color(255, 255, 255));
		// $qrCodeResult = (new PngWriter())->write($qrCode)->getString();

		// $qrcodeTempFile = $tempDir . createUniqueCode() . '.png';

		// (new PngWriter())->write($qrCode)->saveToFile($qrcodeTempFile);

		// 合成图片
		$poster = (new Poster(public_path() . 'upload' . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR .  'c8413bc3eeb5ddf1e417c5f53c07acce46220672cc0d22ffedd6589c5e2abf.jpg'))
			->image('http://192.168.1.150:8053/upload/image/c8413bc3eeb5ddf1e417c5f53c07acce46220672cc0d22ffedd6589c5e2abf.jpg', 100, 100, 100, 100, false)
			// ->image($qrcodeTempFile, 850, 2050, 420, 420)
			->text('添加文字', 20, 40, 50, '255,255,255')
			->toBase64();

		return '<img src="' . $poster . '" />';

		// unlink($qrcodeTempFile);
		var_dump($poster);
	}
}
