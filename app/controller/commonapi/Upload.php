<?php

namespace app\controller\commonapi;

use app\BaseController;
use aliyun\AliyunOss;

/**
 * @api 上传
 *  @AuthComment loginModels ["Admin","User"]
 */
class Upload extends BaseController
{
	protected $middleware = [
		AuthComment::class,
	];


	/**
	 * @api 上传图片
	 * @param string $file 图片文件
	 */
	public function uploadImage()
	{
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', '0');
		$file = request()->file('file');
		if ($file) {
			try {
				validate(['file' => [
					'fileSize' => 30 * 1024 * 1024,
					'fileExt' => 'jpg,jpeg,png,gif',
					'fileMime' => 'image/jpeg,image/png,image/gif',
				]])->check(['file' => $file]);
			} catch (\think\exception\ValidateException $e) {
				return file($e->getMessage());
			}

			$save = AliyunOss::sendFile($file, 'image/', true);
			return success(['url' =>  $save['url']]);
		}
		return failed('请传入文件');
	}

	/**
	 * @api 获取直传OSS的STS凭证
	 */
	public function getUploadOssSTS()
	{
		list($msec, $sec) = explode(' ', microtime());
		$msectime = (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);

		return success([
			'sts' => AliyunOss::getOssSts(),
			'ext' => ['mov', 'mp4', '3gp', 'avi', 'm3u8', 'webm'],
			'size' => ['min' => -1, 'max' => -1],
			// 储存目录，这个值返回给前端js使用
			'path' => 'course/video/' . date('Y/m/') . $msectime,
		]);
	}

	/**
	 * @api 获取OSS某个资源访问凭证
	 * @param string $name 资源完整路径 https://xxx.oss.com/xx/xx.jpg
	 * @param integer $expire_time 凭证过期时长（秒）
	 * @return void
	 */
	public function getObjectAccessToken($name = '', $expire_time = 10)
	{
		try {
			$url = AliyunOss::getOssAccessQuery($name, $expire_time);
		} catch (\Exception $e) {
			return failed($e->getMessage());
		}
		return success([
			'url' => $url,
		]);
	}
}
