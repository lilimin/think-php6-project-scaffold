<?php

namespace app\controller\adminapi;

use app\middleware\AuthComment;
use app\BaseController;
use app\model\{
	SysConfig as ModelSysConfig
};

/**
 * @api 系统配置管理
 * @AuthComment loginModels ["Admin"]
 */
class SysConfig extends BaseController
{
	// 中间
	protected $middleware = [AuthComment::class];

	/**
	 * @api 获取系统配置项
	 * @param string $key 获取指定key，不传获取所有
	 */
	public function getSysConfig($key = null)
	{
		return success(ModelSysConfig::getSysConfig($key));
	}

	/**
	 * @api 设置系统配置项
	 * @param string $key 配置项的key
	 * @param string $value 要设置的值
	 */
	public function setSysConfig($key = '', $value = '')
	{
		$config = ModelSysConfig::getSysConfig($key);
		if (!$config) return failed('配置项不存在');

		validate()->check(['value' => $value], ['value' => $config['type'] == 'json' ? 'require|array' : 'require']);
		$config->save(['value' => $value]);

		return success($config);
	}
}
