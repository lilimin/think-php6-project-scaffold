<?php

namespace app\controller\adminapi;

use app\middleware\AuthComment;
use app\BaseController;
use app\model\{
    User as ModelUser,
};
use FunnelThrottle\Throttle;

use think\exception\ValidateException;

/**
 * @api 用户管理
 * @AuthComment loginModels ["Admin"]
 */
class User extends BaseController
{
    // 中间
    protected $middleware = [AuthComment::class];

    /**
     * @api 获取用户列表
     * @desc [自动分页]、[字段筛选]
     * @param string $search_word 搜索：name|mobile
     */
    public function getUserList($search_word = '', $_with = [])
    {
        $query = ModelUser::with($_with)
            ->filterFields()
            ->pagesort()
            ->when($search_word, fn ($q) => $q->where('name|mobile', 'like', '%' . $search_word . '%'));

        return success([
            'total' => $query->count(),
            'rows' => $query->select(),
        ]);
    }
}
