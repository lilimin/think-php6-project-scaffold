<?php

namespace app\controller\adminapi;

use app\middleware\AuthComment;
use app\BaseController;
use app\model\Admin as MAdmin;
use throttling\ThrottlingFunnel;

use think\exception\ValidateException;
use think\facade\Cache;

/**
 * @api 管理员
 * @AuthComment loginModels ["Admin"]
 */
class Admin extends BaseController
{
    // 中间
    protected $middleware = [AuthComment::class];

    /**
     * @api 登陆
     * @desc 默认帐号admin<br/>默认密码123456<br/>
     * @param string $account 帐号
     * @param string $pswd
     * @AuthComment loginModels []
     */
    public function login($account = '', $pswd = '')
    {
        $thro = new ThrottlingFunnel('AdminLogin:' . request()->ip(), [
            'out_dely' => 30,
            'max_count' => 10,
        ]);
        if (!$thro->inc()) {
            return failed('操作太频繁，请稍后再试');
        }

        $admin = MAdmin::where('account', $account)->find();
        if (!$admin) return failed('管理员不存在');
        if (!$admin->checkPswd($pswd)) return failed('密码错误');

        $admin->updateLoginInfo(); // 记录登陆信息

        return success($admin->getLoginToken());
    }

    /**
     * @api 获取管理员详情
     * @desc 使用全局token，无需传参
     */
    public function getDetail()
    {
        return success($this->request->AuthComment->getLoginModel('Admin'));
    }

    /**
     * @api 修改密码
     * @param string $pswd 新密码
     */
    public function changePswd($pswd = '')
    {
        try {
            $this->validate(['pswd' => $pswd], ['pswd|密码' => 'require|min:6|alphaDash']);
        } catch (ValidateException $e) {
            return failed($e->getError());
        }

        $admin = $this->request->AuthComment->getLoginModel('Admin');
        $admin->savePswd($pswd);
        return success();
    }
}
