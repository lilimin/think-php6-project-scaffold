<?php
declare (strict_types = 1);

namespace app\controller\adminapi;

use app\BaseController;
use app\middleware\AuthComment;
use think\exception\ValidateException;
use think\facade\Db;
use app\model\{
    AdminRole as ModelAdminRole
};

/**
 * @api 管理员角色
 * @AuthComment loginModels ["Admin"]
 */
class AdminRole extends BaseController
{
    //中间件
    protected $middleware = [AuthComment::class];

    /**
     * @api 获取管理员角色列表
     * @param string $search_word 搜索名称
     */
    public function getAdminRoleList($search_word = '')
    {
        $result = ModelAdminRole::scope('Pagesort')->scope('FilterFields', [])->scope('WithAppends', [])
            ->when($search_word, fn ($q) => $q->where('name|id', 'like', '%' . $search_word . '%'));
        
        $total = $result->count();
        $rows = $result->select();

        return success(['total' => $total, 'rows' => $rows]);
    }


    /**
     * @api 编辑管理员角色
     * @param number $id 
     * @param string $name 名称 
     * @param string $test_json 测试JSON字段 
     *
     */
    public function editAdminRole($id = '')
    {
        if ($id) {
            $item = ModelAdminRole::find($id);
            if (!$item) return failed('该管理员角色不存在');
        } else {
            $item = new ModelAdminRole();
        }

        $params = request()->only(['name','test_json']);
        validate()->check($params, [
            'name|名称' => 'require|max:255',
            
        ]);

        $item->save($params);
        return success($item);
    }

    /**
     * @api 删除管理员角色
     * @param string $id
     */
    public function delAdminRole($id = '')
    {
        $item = ModelAdminRole::find($id);
        if (!$item) return failed('该管理员角色不存在');

        $item->delete();

        return success();
    }
}
