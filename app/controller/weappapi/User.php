<?php

namespace app\controller\weappapi;

use app\BaseController;
use app\middleware\AuthComment;
use app\model\{
	User as MUser
};

/**
 * @api 用户登陆类
 * @AuthComment loginModels ["User"]
 * @desc 常用的用户登陆示例
 */
class User extends BaseController
{
	// 中间件
	protected $middleware = [AuthComment::class];

	/**
	 * @api 小程序code登录
	 * @AuthComment loginModels []
	 */
	public function code2Token($code = '')
	{
		$app = app('use_wechat')->getMiniApp();

		$utils = $app->getUtils();
		try {
			$data = $utils->codeToSession($code);
		} catch (\Exception $e) {
			return failed($e->getMessage());
		}
		$user = MUser::where('openid', $data['openid'])->find();

		if (!$user) {
			// 新增用户
			$user = MUser::find(MUser::create([
				'openid' => $data['openid'],
				'unionid' => $data['unionid'] ?? '',
				'session_key' => $data['session_key'],
				'nickname' => '微信用户', // 默认昵称
				'avatar' => '', // 默认头像
			])['id']);
		} else {
			$user->save(['session_key' => $data['session_key']]);
		}

		$user->updateLoginInfo();

		return success(['token' => $user->getLoginToken()]);
	}

	/**
	 * @api 获取用户信息
	 */
	public function getInfo()
	{
		$user = $this->request->AuthComment->getLoginModel('User');

		return success($user->append([]));
	}
}
