<?php

declare(strict_types=1);

use think\response\Json;

// 应用公共文件

if (!function_exists('success')) {
	/**
	 * 响应成功消息
	 */
	function success($data = null, string $msg = 'ok', int $code = 0): Json
	{
		return json(compact('msg', 'code', 'data'));
	}
}

if (!function_exists('failed')) {
	/**
	 * 响应失败消息
	 */
	function failed(string $msg = 'failed', int $code = 1, $data = null): Json
	{
		return json(compact('msg', 'code', 'data'));
	}
}

if (!function_exists('createUniqueCode')) {
	/**
	 * 创建指定长度的唯一编号（年月日时分秒+随机数字）
	 * 例如订单号等
	 * @param integer $num 随机数字长度
	 */
	function createUniqueCode($num = 6): string
	{
		$strand = str_pad((string)mt_rand(1, 99999), $num, "0", STR_PAD_LEFT);
		return date('YmdHsi') . $strand;
	}
}

if (!function_exists('arrayIsPureNumber')) {
	/**
	 * 数组是否是纯数字组成
	 * @param array $array
	 */
	function arrayIsPureNumber($array): bool
	{
		if (!is_array($array)) return false;
		foreach ($array as $k => $v) {
			if (!is_numeric($k)) return false;
		}
		return true;
	}
}

if (!function_exists('strCutBetween')) {
	/**
	 * 字符串截取指定两个字符之间的内容
	 * @param string $begin 开始标记
	 * @param string $end 结束标记
	 * @param string $str 原字符串
	 */
	function strCutBetween($begin, $end, $str): string
	{
		$b = mb_strpos($str, $begin) + mb_strlen($begin);
		$e = mb_strpos($str, $end) - $b;
		return mb_substr($str, $b, $e);
	}
}

if (!function_exists('arrayDeleteSort')) {
	/**
	 * 删除数组中某个元素，并重新排列（整理下标）
	 * @param array $array
	 * @param mixed $index 要删除元素的下标（若传入null，则忽略下标，采用第三个参数方式）
	 * @param array $value 要删除元素的值（若第二个参数传入null，则本参数起作用）
	 */
	function arrayDeleteSort($array, $index = 0, $value = null): array
	{
		if (is_null($index)) {
			$result = array_filter($array, fn ($it) => $it !== $value);
		} else {
			$result = $array;
			unset($result[$index]);
		}
		return array_merge($result);
	}
}

if (!function_exists('hanGetFirstLetter')) {
	/**
	 * 获取汉字拼音首字母
	 * @param string $s0 汉字字符串
	 */
	function hanGetFirstLetter($s0): string
	{
		$fchar = ord(substr($s0, 0, 1));
		if (($fchar >= ord("a") and $fchar <= ord("z")) or ($fchar >= ord("A") and $fchar <= ord("Z"))) return strtoupper(chr($fchar));
		$s = iconv("UTF-8", "GBK", $s0);
		$asc = ord($s[0]) * 256 + ord($s[1]) - 65536;
		if ($asc >= -20319 and $asc <= -20284) return "A";
		if ($asc >= -20283 and $asc <= -19776) return "B";
		if ($asc >= -19775 and $asc <= -19219) return "C";
		if ($asc >= -19218 and $asc <= -18711) return "D";
		if ($asc >= -18710 and $asc <= -18527) return "E";
		if ($asc >= -18526 and $asc <= -18240) return "F";
		if ($asc >= -18239 and $asc <= -17923) return "G";
		if ($asc >= -17922 and $asc <= -17418) return "H";
		if ($asc >= -17417 and $asc <= -16475) return "J";
		if ($asc >= -16474 and $asc <= -16213) return "K";
		if ($asc >= -16212 and $asc <= -15641) return "L";
		if ($asc >= -15640 and $asc <= -15166) return "M";
		if ($asc >= -15165 and $asc <= -14923) return "N";
		if ($asc >= -14922 and $asc <= -14915) return "O";
		if ($asc >= -14914 and $asc <= -14631) return "P";
		if ($asc >= -14630 and $asc <= -14150) return "Q";
		if ($asc >= -14149 and $asc <= -14091) return "R";
		if ($asc >= -14090 and $asc <= -13319) return "S";
		if ($asc >= -13318 and $asc <= -12839) return "T";
		if ($asc >= -12838 and $asc <= -12557) return "W";
		if ($asc >= -12556 and $asc <= -11848) return "X";
		if ($asc >= -11847 and $asc <= -11056) return "Y";
		if ($asc >= -11055 and $asc <= -10247) return "Z";
		return null;
	}
}

if (!function_exists('rsaEncode')) {
	/**
	 * RSA分段加密
	 * @param string $public_key 公钥内容
	 * @param string $data 要加密的数据
	 * @throws Exception
	 */
	function rsaEncode(string $public_key, string $data): string
	{
		$crypto = '';
		foreach (str_split($data, 117) as $chunk) {
			$encryptData = '';
			if (openssl_public_encrypt($chunk, $encryptData, $public_key)) {
				$crypto .= $encryptData;
			} else {
				throw new \Exception('RSA加密失败');
			}
		}
		return base64_encode($crypto);
	}
}

if (!function_exists('rsaDecode')) {
	/**
	 * RSA分段解密
	 * @param string $private_key 私钥内容
	 * @param string $data 密文
	 * @throws Exception
	 */
	function rsaDecode(string $private_key, string $data): string
	{
		$decryptData = '';
		$crypto = '';
		foreach (str_split(base64_decode($data), 128) as $chunk) {
			if (openssl_private_decrypt($chunk, $decryptData, $private_key)) {
				$crypto .= $decryptData;
			} else {
				throw new \Exception('RSA解密失败');
			}
		}
		return $crypto;
	}
}

if (!function_exists('curlPost')) {
	/**
	 * curl发起Post请求
	 * @param string $url
	 * @param array $data
	 * @param array $header
	 */
	function curlPost($url = '', array $data = [], $header = [])
	{
		$data_string = json_encode($data, JSON_UNESCAPED_UNICODE);

		// $data_string = $data;
		$curl_con = curl_init();
		curl_setopt($curl_con, CURLOPT_URL, $url);
		curl_setopt($curl_con, CURLOPT_HEADER, false);
		curl_setopt($curl_con, CURLOPT_POST, true);
		curl_setopt($curl_con, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($curl_con, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt(
			$curl_con,
			CURLOPT_HTTPHEADER,
			array_merge([
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string)
			], $header)
		);
		curl_setopt($curl_con, CURLOPT_POSTFIELDS, $data_string);
		$res = curl_exec($curl_con);
		// $status = curl_getinfo($curl_con);
		curl_close($curl_con);
		return $res;
	}
}

if (!function_exists('curlGet')) {
	/**
	 * curl发起Get请求
	 * @param string $url
	 * @param array $data [TODO]未实现参数拼接
	 * @param array $header
	 */
	function curlGet($url = '', array $data = [], $header = [])
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($curl, CURLOPT_FAILONERROR, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($curl, CURLOPT_HEADER, true);
		if (1 == strpos("$" . $url, "https://")) {
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		}
		return curl_exec($curl);
	}
}
