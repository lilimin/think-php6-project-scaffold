<?php

declare(strict_types=1);

namespace app\model;

use think\model\concern\SoftDelete;
use app\model\common\BaseModel;
use app\model\common\HasPswdTrait;
use app\model\common\LoginCacheTrait;

/**
 * 管理员
 */
class Admin extends BaseModel
{
    public static $modelAlias = '管理员';

    protected $table = 'admin';
    protected $pk = 'id';

    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    protected $json = [];
    protected $jsonAssoc = true;

    protected $hidden = ['delete_time', 'pswd', 'pswd_salt'];

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        // 配置登陆缓存状态
        $this->setLoginCacheOption([
            'expire_time' => 7200, //2 小时
        ]);
    }

    /** 写入后事件 */
    public static function onAfterWrite($model)
    {
        // 更新数据到缓存中
        $model->updateLoginCache(true);
    }

    /**
     * 储存登录信息
     */
    public function updateLoginInfo(): self
    {
        $this->save([
            'last_login_ip' => request()->ip(),
            'last_login_time' => date('Y-m-d H:i:s'),
        ]);
        return $this;
    }
}
