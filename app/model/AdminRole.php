<?php

declare(strict_types=1);

namespace app\model;

use think\model\concern\SoftDelete;
use app\model\common\BaseModel;
use app\model\common\HasPswdTrait;
use app\model\common\LoginCacheTrait;

/**
 * 管理员角色
 */
class AdminRole extends BaseModel
{
    public static $modelAlias = '管理员角色';

    protected $table = 'admin_role';
    protected $pk = 'id';

    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    protected $json = ['test_json'];
    protected $jsonAssoc = true;

    protected $hidden = ['delete_time'];

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        
    }

    /** 写入后事件 */
    public static function onAfterWrite($model)
    {
        
    }

    
}
