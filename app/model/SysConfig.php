<?php

namespace app\model;

use app\model\common\BaseModel;

class SysConfig extends BaseModel
{
	protected $table = 'sys_config';
	protected $pk = 'id';

	protected $hidden = [];

	const TYPE_MAP = [
		'string' => '',
		'number' => '',
		'json' => '',
	];

	public static function getSysConfig($key = null)
	{
		if (!$key) return static::select();
		return static::where('key', $key)->find();
	}

	public function setValueAttr($value, $data)
	{
		if ($data['type'] == 'string') {
			return $value;
		} else if ($data['type'] == 'number') {
			return $value;
		} else if ($data['type'] == 'json') {
			return json_encode($value, JSON_UNESCAPED_UNICODE);
		}
	}

	public function getValueAttr($value, $data)
	{
		if ($data['type'] == 'string') {
			return $value;
		} else if ($data['type'] == 'number') {
			$int = intval($value);
			$float = floatval($value);
			return $int == $float ? $int : $float;
		} else if ($data['type'] == 'json') {
			return json_decode($value, true);
		}
	}
}
