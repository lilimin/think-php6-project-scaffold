<?php

namespace app\model\common;

use think\Model;

/**
 * 基础模型
 */
abstract class BaseModel extends Model
{
	/**
	 * 自动处理分页参数
	 * 规定：当前页码index、每页条数limit、排序字段sort_key、排序类型sort_type
	 * 注意：当和join一起使用时，请先设置别名，如  Model::alias('a')->pagesort()->join(...)->select()
	 * 用法： Model::pagesort()->where(xxx)->select() 或 $model->pagesort()->select();
	 */
	public function scopepagesort(\think\db\Query $query): \think\db\Query
	{
		$table = $query->getTable();
		$options = $query->getOptions();
		$alias = '';
		if (isset($options['alias']) && is_array($options['alias'])) {
			foreach ($options['alias'] as $k => $v) {
				if ($k == $table) {
					$alias = $v . '.';
				}
			}
		};

		// $alias 会被设为空字符串或  alias. 可直接拼接到后续语句中
		$request = request();

		$sort_key = $request->param('sort_key');
		$sort_type = in_array(strtoupper($request->param('sort_type')), ['ASC', 'DESC']) ? strtoupper($request->param('sort_type')) : 'DESC';
		$limit = $request->param('limit/d');
		$index = $request->param('index/d');
		if ($limit) {
			$index = $index ? intval($index) : 1;
			$query->page($index, intval($limit));
		}
		if ($sort_key) {
			$fields = $query->getTableFields();
			if (in_array($sort_key, $fields)) {
				// 原始字段排序
				$query->order($alias . $sort_key . ' ' . $sort_type . ',id desc');
			}
		} else {
			$query->order('id', 'desc');
		}
		return $query;
	}


	/**
	 * json数组字段 查询构造器
	 *
	 * @param [type] $query query类
	 * @param String $key 字段名
	 * @param [type] $value 查询的值，例如 1，[1,2]，["id"=>1]，["id"=>"1","name"=>"hhh"]，["user"=>["age"=>18]]
	 * @param string $where 查询方式（AND或OR）
	 * @return void
	 */
	public function scopeQueryJsonArray($query, String $key, $value, String $where = 'AND')
	{
		if (is_array($value)) {
			//是纯索引数组用JSON_ARRAY，否则用JSON_OBJECT
			if (arrayIsPureNumber($value)) {
				$value = array_map(fn ($v) => is_numeric($v) ? $v : "'$v'", $value);
				$value_result = implode(',', $value);
				$json_fun = 'JSON_ARRAY(' . $value_result . ')';
			} else {
				//递归生成json object格式
				function jsonObjcet($value)
				{
					$result = [];
					foreach ($value as $k => $v) {
						$list = '';
						$list .= "'$k',";
						$v_result = $v;
						if (is_array($v)) {
							$v_result =  jsonObjcet($v);
						}
						if (!is_int($v_result) && !str_starts_with($v_result, 'JSON_OBJECT')) $v_result = "'$v_result'";
						$list .= $v_result;
						$result[] = $list;
					}
					return 'JSON_OBJECT(' . implode(',', $result) . ')';
				}

				$json_fun = jsonObjcet($value);
			}
		} else {
			$value = is_numeric($value) ? $value : "'$value'";
			$json_fun = 'JSON_ARRAY(' . $value . ')';
		}

		$query->whereRaw('JSON_CONTAINS(' . $key . ',' . $json_fun  . ')', [], $where);
	}

	/** 
	 * 自动字段筛选
	 * 根据模型的 allowFields() 获取字段，具体逻辑参见该方法
	 * 根据 allowFields 从 Request 中获取对应同名参数，参数支持 数组、非数组
	 * 数组：用whereIn查询，其中空数组会被忽略
	 * 非数组：直接用where查询
	 * @param array $ignoreFields 要忽略的字段
	 */
	public function scopeFilterFields($query, $ignoreFields = [])
	{
		$request = request();
		$param = $request->only(array_diff($this->checkAllowFields(), $ignoreFields));
		foreach ($param as $key => $val) {
			is_array($val)
				? $query->when(!empty($val), fn ($q) => $q->whereIn($key, $val))
				: $query->where($key, $val);
		}
		return $query;
	}

	/**
	 * 自动设置字段with\append\hidden
	 * 前端提交 _with _append _hidden字段
	 * @param array $ignoreFields 要忽略的字段
	 */
	public function scopeWithAppends($query, $ignoreFields = [])
	{
		$request = request();
		$with = array_diff($request->param('_with') ?? [], $ignoreFields);
		$append = array_diff($request->param('_append') ?? [], $ignoreFields);
		$hidden = array_diff($request->param('_hidden') ?? [], $ignoreFields);
		return $query->with($with)->append($append)->hidden($hidden);
	}
}
