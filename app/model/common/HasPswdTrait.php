<?php

declare(strict_types=1);

namespace app\model\common;

use think\Model;

/**
 * 可使用帐号密码登陆
 * 要求有 pswd pswd_salt 两个字段
 */
trait HasPswdTrait
{

	/**
	 * 设置密码并保存到数据库
	 * @param string $pswd 密码
	 */
	public function savePswd(string $pswd)
	{
		$salt = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
		$this->save(['pswd' => md5($pswd . $salt), 'pswd_salt' => $salt]);
		return $this;
	}

	/**
	 * 检查密码是否正确
	 * @param string $pswd 密码
	 */
	public function checkPswd(string $pswd)
	{
		return $this['pswd'] == md5($pswd . $this['pswd_salt']);
	}
}
