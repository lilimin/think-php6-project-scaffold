<?php

declare(strict_types=1);

namespace app\job;

use think\queue\Job;
use think\facade\Log;

class JobDemo
{

	public function fire(Job $job, $payload)
	{
		//....这里执行具体的任务 

		if ($job->attempts() > 3) {
			//通过这个方法可以检查这个任务已经重试了几次了
		}

		//如果任务执行成功后 记得删除任务，不然这个任务会重复执行，直到达到最大重试次数后失败后，执行failed方法
		$job->delete();

		// 也可以重新发布这个任务
		// $job->release(3); //$delay为延迟时间
	}

	/**
	 * 任务达到最大重试次数后，标记为完全失败
	 * 此时若本方法存在，会被调用，若不存在也不影响。若在config中配置了 queue.failed 参数，则会启用自动记录任务到错误表中
	 */
	public function failed($payload, \Exception $exception)
	{
		// 这里建议写入独立级别的日志
		// Log::write('xxxxx', 'job-failed');
	}
}
