## 用法说明

#### 概念

队列是基于一个储存媒介来储存多个Job
**队列可以是多条，以队列名queue来区分，每个队列中有若干的Job**
使用Redis实现队列比较简单，/vendor/topthink/think-queue 中的README.md实现即可

由于生产环节中Redis通常用于缓存，资源不充沛的情况下，使用MySQL比较实用
下面重点讨论MySQL队列

## 安装和配置think-queue

### 1. 安装

```
composer require topthink/think-queue
```

### 2. 创建数据库表

其中表名可以自定义，通常跟项目业务表放在一个数据库中，共用一个mysql连接配置

```
# 创建队列表
CREATE TABLE `queue_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务名',
  `attempts` tinyint(5) unsigned NOT NULL DEFAULT '0' COMMENT '重试次数',
  `reserve_time` int(10) unsigned DEFAULT NULL COMMENT '预留时间',
  `available_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '可用时间',
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '数据（Json）',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci;

# 创建队列失败记录表
CREATE TABLE `queue_job_failed`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `fail_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci;
```

### 3. 创建配置文件

通常在 config/ 中已经存在 queue.php 配置文件了
如果不存在，可以去 vendor/topthink/think-queue 中复制它的 config.php

```
return [
    'default'     => 'database',
    'connections' => [
        'database' => [
            'type'       => 'database',
            // 默认队列名，对应mysql表中的queue字段，对应 Queue::push() 的第三个参数
            'queue'      => 'default',
            // 队列表名
            'table'      => 'queue_job',
            // connection配置的是连接名，必须是字符串。
            // 对应 config.database.connections 中的连接名
            // 要注意 对env的修改，如果配置写在了 .env.development 中
            // 则需要修改 /think 文件，保持跟 /public/index.php 一致
            'connection' => 'mysql',
        ],
    ],
    // 队列失败这里也要配置
    'failed'      => [
        // 类型也设置为database
        'type'  => 'database',
        // 设置队列失败表的表名
        'table' => 'queue_job_failed',
    ],
];
```

### 4. 创建队列

> `think\facade\Queue::push($job, $data = '', $queue = null)` 和 `think\facade\Queue::later($delay, $job, $data = '', $queue = null)` 两个方法，前者是立即执行，后者是在`$delay`秒后执行

`$job` 是任务名  
单模块的，且命名空间是`app\job`的，比如上面的例子一,写`Job1`类名即可  
多模块的，且命名空间是`app\module\job`的，写`model/Job1`即可  
其他的需要些完整的类名，比如上面的例子二，需要写完整的类名`app\lib\job\Job2`  
如果一个任务类里有多个小任务的话，如上面的例子二，需要用@+方法名`app\lib\job\Job2@task1`、`app\lib\job\Job2@task2`

`$data` 是你要传到任务里的参数

`$queue` 队列名，指定这个任务是在哪个队列上执行，同下面监控队列的时候指定的队列名,可不填


### 5. 处理队列

参考JobDemo.php

**这里重点强调队列的failed逻辑**

在fire()方法中，以下几种情况会产生如下结果：
1. $job->delete();
   
    任务直接被删除，即从job表删除，不在failed表中留存记录

2. $job->failed();

    任务被记为失败，attach++，并再次入队

3. 抛出异常

    任务被记为失败，attach++，并再次入队

如果想让一个$job完全的失败，并自动计入failed表，唯一的方法是其attach值超过Worker的最大重试次数。Worker的最大重试次数只能在启动时通过 --tries 参数决定。


### 6. 启动队列监听

使用命令行工具
```
# 开始处理队列
php think queue:listen
php think queue:work

关于listen和work的区别
listen —— 开发用：会监听job类的文件代码变化，无需重启队列进程，修改文件随时生效，性能较差
work —— 生产用：不会监听job类的文件代码变化，每次修改需要重启进程

# 处理指定的队列
php think queue:work --queue QueueName

# 查看说明
php think

# 查看某个操作的说明
php think queue:work -h

```

[注意事项] 如果要使用队列失败的功能（任务失败后自动存入失败任务表），必须在启动时设置 --tries 参数，声明任务最大重试次数。该参数默认是0，不限制。可以通过 php think queue:work -h 查看说明



### 6. 守护进程

在宝塔中可使用插件 [宝塔应用管理器]，非宝塔可用 [Supervisor]