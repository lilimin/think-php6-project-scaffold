<?php

declare(strict_types=1);

namespace openapi;

use openapi\oas\OAS;
use openapi\oas\Service;
use openapi\oas\path\Request;
use openapi\oas\path\Parameter;
use openapi\oas\path\Schema;

class ApiDoc
{
	/**
	 * 存放所有包含的php类
	 * 由于内部需要递归目录，必须用数组暂存合并
	 * @var array
	 */
	public $classes = [];


	public function __construct($dirs = null)
	{
		if (is_string($dirs)) $dirs = [$dirs];
		else if (is_null($dirs)) $dirs = [];

		foreach ($dirs as $dir) {
			$this->parseDirToClass($dir);
		}
	}

	/**
	 * 扫描一个目录
	 * 递归其中的子目录，把所有php文件转为命名空间类存入classes
	 * @param string $dir 目录路径
	 * @param string $dirNamespace 用于替换目录所在的根命名空间，TP6中使用app，TP5使用application
	 * @return $this
	 */
	public function parseDirToClass(string $dir, $dirNamespace = 'app')
	{
		$dir = str_replace('/', DIRECTORY_SEPARATOR, $dir);
		$reader = opendir($dir);
		$arrays = [];
		while ($filename = readdir($reader)) {
			if ($filename == '.' || $filename == '..') continue;
			$full_path = $dir . DIRECTORY_SEPARATOR . $filename;
			if (is_dir($full_path)) {
				$this->parseDirToClass($full_path);
			} else {
				$namespace = substr(strstr($full_path, $dirNamespace), strlen($dirNamespace . DIRECTORY_SEPARATOR));
				$namespace = str_replace('/', '\\', substr($namespace, 0, strrpos($namespace, DIRECTORY_SEPARATOR)));
				$classname = $dirNamespace . '\\' . $namespace . '\\' . substr($filename, 0, strrpos($filename, '.'));
				$arrays[] = $classname;
			}
		}
		$this->classes = array_merge($this->classes, $arrays);
		return $this;
	}

	/**
	 * 把所有classes转为api输出
	 */
	public function parseClassToApi()
	{
		$apis = [];
		foreach ($this->classes as $class) {
			$ref = new \ReflectionClass($class);
			$classComment = $this->parseComment($ref->getDocComment());

			// 类的分组名（API组名）
			$classTagName = '';
			foreach ($classComment as $comment) {
				if ($comment[0] == 'api') {
					$classTagName = $comment[1];
					break;
				}
			}
			if (!$classTagName) continue;

			// 所有api
			$methods = $ref->getMethods();
			foreach ($methods as $method) {

				$comments = $this->parseComment($method->getDocComment());
				$commentApi = null;
				// 方法必须包含一个 @api 注释，否则过滤
				foreach ($comments as $comment) {
					if ($comment[0] == 'api') {
						$commentApi = $comment;
						break;
					}
				}
				if (is_null($commentApi)) continue;

				// 这里得到每一个合格的api方法， @commentApi 就是这一条@api注释
				$explodsClass = explode('\\', $class);
				$path = (isset($explodsClass[2]) ? '/' . $explodsClass[2] : '')
					. (isset($explodsClass[3]) ? '/' . $explodsClass[3] : '')
					. ('/' . $method->name);

				$requestMethod = 'GET';
				
				$request = new Request;
				$request->tags = [$classTagName];
				// 支持两种写法
				// @api POST 接口名称
				// @api 接口名称
				if(count($commentApi) > 2){
					$request->summary = $commentApi[2];
					$requestMethod = strtoupper($commentApi[1]);
				}else{
					$request->summary = $commentApi[1];
				}

				// 遍历每一行注释@
				foreach ($comments as $comment) {
					// api描述
					if ($comment[0] == 'desc') {
						$request->description = implode(' ',array_slice($comment, 1));
					}
					// 每个参数
					if ($comment[0] == 'param') {
						$param = new Parameter;
						$param->name = substr($comment[2], 1);
						$param->description = $comment[3] ?? '';
						$param->in = 'query';

						$schema = new Schema;
						$schema->type = $comment[1];
						$schema->example = $comment[4] ?? null;
						$param->schema = $schema;

						$param->required = is_null($schema->example);

						$request->parameters[] = $param;
					}
				}

				// 处理请求方法
				$apis[$path] = [$requestMethod => $request];
			}
		}

		// 整个api的说明
		$oas = new OAS();
		$oas->info->title = '项目名';
		$oas->info->description = '项目说明';
		$oas->info->version = 'v1.0.0';

		$service = new Service();
		$host = config('app.app_host');
		$url = request()->scheme() . '://' . $host . ':' . request()->port();
		$service->url = $url;
		$service->description = '测试环境';
		$oas->servers[] = $service;

		$oas->paths = $apis;

		return $oas;
	}

	/**
	 * 解析commit
	 * 把一个@开头的注释行按空格分割成数组
	 * @param string $comment
	 */
	protected function parseComment($comment)
	{
		if (!is_string($comment)) return [];

		$line_array = explode(PHP_EOL, $comment);
		$result = [];
		foreach ($line_array as $line) {

			// 过滤无@开头的项
			if (strpos($line, '@') === false) continue;

			// 移除@之前的符号 [/ * @title 标题] ===> [title 标题]
			$res = preg_replace("/.+@/", '', $line);

			// 除去多个空格的情况 [param int id  商品ID] ===> [param int id 商品ID]
			$res = preg_replace("/\s+/", ' ', $res);

			// 按空格分隔 ['param', 'int', 'id', '商品ID'], 
			$result[] = explode(' ', $res);
		}
		return $result;
	}
}
