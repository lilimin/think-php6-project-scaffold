<?php

declare(strict_types=1);

namespace openapi\oas;

class Service
{
	public $url = '';
	public $description = '';
}
