<?php

declare(strict_types=1);

namespace openapi\oas;

class Info
{
	public $title = '';
	public $description = '';
	public $version = '';
}
