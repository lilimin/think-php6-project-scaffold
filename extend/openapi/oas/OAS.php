<?php

declare(strict_types=1);

namespace openapi\oas;

/**
 * 顶层对象
 */
class OAS
{
	public $openapi = '3.0.0';
	public $info;
	public $servers = [];
	public $paths = [];

	public function __construct()
	{
		$this->info = new Info();
		$this->paths = [];
	}

	public function toJson()
	{
		return json_encode($this, JSON_UNESCAPED_UNICODE);
	}
}
