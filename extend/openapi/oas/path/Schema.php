<?php

declare(strict_types=1);

namespace openapi\oas\path;

class Schema
{
	public $type = '';
	public $format = '';
	public $example = null;
	public $pattern = '';
	public $minLength = '';
	public $maxLength = '';
	public $items = [];
}
