<?php

declare(strict_types=1);

namespace openapi\oas\path;


class Request
{
	public $tags = [];
	public $summary = '';
	public $description = '';
	public $operationId = '';
	public $parameters = [];
	public $responses = [];
}
