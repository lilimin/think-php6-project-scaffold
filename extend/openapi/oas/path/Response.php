<?php

declare(strict_types=1);

namespace openapi\oas\path;


class Response
{
	public $description = '';
	public $content = [];
}
