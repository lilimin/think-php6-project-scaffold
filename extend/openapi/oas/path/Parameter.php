<?php

declare(strict_types=1);

namespace openapi\oas\path;

class Parameter
{
	public $name = '';
	/**
	 * 参数位置
	 * path/query/header/cookie
	 * @var string
	 */
	public $in = '';
	public $description = '';
	public $required = false;
	public $schema;
}
