## OpenApiDoc类

open-api规范是由swagger主导，目前最新3.0版本
官方规范文档：https://swagger.io/specification/
翻译文档：https://www.jianshu.com/p/5365ef83252a


## 用法说明

1. 放在extend中的独立类库，用于自动根据注解生成API文档数据（OAS3规范）

2. 创建生成API文档的接口
```php
# app/controller/Index.php

use app\BaseController;
use openapi\ApiDoc;

class Index extend BaseController
{
	/**
	 * 获取一组API文档
	 */
	public function getApiDoc()
	{
		// 传入本目录路径，会扫描本目录下所有文件和子目录，输出为OAS对象
		$oas = (new ApiDoc(__DIR__))->parseClassToApi();
		// dump($oas);

		// 如需转json
		return json($oas);
	}

	/**
	 * 获取接口文档分组信息
	 * 这是一个示例
	 */
	public function getApiGroup()
	{
		$host = config('app.app_host'); // 当前的host_name
		$url = request()->scheme() . '://' . $host . ':' . request()->port();

		return json([
			[
				'name' => '用户端',
				'link' => $url . '/index/getApiDocJson?group=weappapi'
			],
			[
				'name' => '后台接口',
				'link' => $url . '/index/getApiDocJson?group=adminapi'
			],
		]);
	}
}

```

3. API文档的前端在 {HOST}/apidoc/index.html，加一个参数?group={HOST}/index/getApiGroup。即完整的访问 `{HOST}/apidoc/index.html?group={HOST}/index/getApiGroup` 就可以看到所有API文档了

## 注解写法规则

用VScode的同学可以装 PHP DocBlocker 插件， 以一个控制器为例

```php
# app/controller/weappapi/User

<?php

namespace app\controller\weappapi;

use think\Controller;

/**
 * @api 用户相关
 * 上一行注解表示本类是API接口，如果没有这个注释本类不会被扫描并纳入API接口
 */
class User extends Controller
{
	/**
	 * @api 获取用户信息
	 * @api POST 获取用户信息
	 * 本方法是一个接口，名称是【获取用户信息】。上面两种写法都可以，注意空格分割，第二种方法声明请求方式是POST，第一种默认是GET
	 * 
	 * @param string $token 用户的TOKEN abc123
	 * 上方声明了一个参数，空格分隔，分别是 类型、参数名、描述、默认值（没有就表示必填）
	 * 
	 * @desc 本接口的备注和说明
	 * 上方声明了本接口的备注说明，备注说明支持空格
	 */
	public function getUserInfo($token = '')
	{
		// ...
	}

}
```

## 注意事项

默认情况是针对TP6单模块模式，多级控制器，自动路由规则来生成的。如果与项目实际情况不符，可以对`openapi\ApiDoc`做个性化修改


