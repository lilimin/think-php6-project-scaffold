<?php

declare(strict_types=1);

namespace throttling\exception;

use RuntimeException;

class ThrottleForbiddenException extends RuntimeException
{
	protected string $uniqueKey;
	protected int $count;
	protected int $maxCount;
	protected int $outDely;

	public function __construct(string $uniqueKey, int $count, int $maxCount, int $outDely)
	{
		$this->uniqueKey = $uniqueKey;
		$this->count = $count;
		$this->maxCount = $maxCount;
		$this->outDely = $outDely;
		$this->message = 'Throttle forbidden because max count is ' . $maxCount . ', but now has ' . $count . '.';
	}

	public function getUniqueKey()
	{
		return $this->uniqueKey;
	}
	public function getCount()
	{
		return $this->count;
	}
	public function getMaxCount()
	{
		return $this->maxCount;
	}
	public function getOutDely()
	{
		return $this->outDely;
	}
}
