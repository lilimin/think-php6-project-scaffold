<?php

declare(strict_types=1);

namespace coder;

use think\console\{
	Command as ThinkCommand,
	Input,
	Output,
	input\Argument,
	input\Option
};
use think\facade\Db;


class Command extends ThinkCommand
{
	/**
	 * 配置命令
	 */
	protected function configure()
	{
		parent::configure();

		/**
		 * 指令和参数的说明
		 * 例如: php think example AA BB --opt=ccc -o ccc
		 * Argument - 指令，其中 example AA BB 三个都是指令，按顺序传入，空格分割
		 * Option	- 参数，其中 --opt=ccc 是参数的完整写法，或者简写 -o ccc (前提是该参数配置了$sort_name)
		 */
		$this->setName('coder')
			->setDescription('根据MySQL的表自动生成模型、Admin端的控制器、vue相关文件');
	}

	/**
	 * 执行
	 */
	protected function execute(Input $input, Output $output)
	{
		$tables = $this->getDbTables();
		foreach ($tables as $table) {
			Coder::genModel($table['TABLE_NAME'], $table['TABLE_COMMENT'] ?: '未定义模型', true);
			if (!in_array($table['TABLE_NAME'], ['admin', 'user'])) {
				Coder::genController($table['TABLE_NAME'], $table['TABLE_COMMENT'] ?: '未定义模型');
			}
		}
	}

	/**
	 * 获取所有表
	 * 能获得 表名TABLE_NAME、表注释TABLE_COMMENT、字段信息TABLE_FIELDS
	 * 依据env的数据库配置
	 */
	private function getDbTables()
	{
		$databaseName = env('database.database');
		return Db::query("SELECT table_name, table_comment FROM information_schema.TABLES WHERE table_schema = '{$databaseName}' ORDER BY table_name");
	}
}
