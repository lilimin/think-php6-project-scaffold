<?php

/**
 * 配置文件示例
 * 使用时可改名为 aliyun.php 放入 config 目录
 */

return [
	'accessKeyId' => '',
	'accessKeySecret' => '',

	'oss' => [
		// oss-cn-shanghai.aliyuncs.com
		'endpoint' => '',
		// gwxt-oss-main
		'bucket' => '',
	],

	'sts' => [
		// oss-cn-shanghai
		'regionId' => '',
	]
];
