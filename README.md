ThinkPHP 6 项目脚手架
=====================

#### 简介

本项目基于 ThinkPHP 6.0.* ，框架详情参阅 [TP6文档](https://www.kancloud.cn/manual/thinkphp6_0/1037479)


#### 特性

* 单应用模式，基于多级控制器实现了不同端的接口隔离，模型等均可共用

* 支持多env文件，参考vue的env模式

* 登陆鉴权中间件，支持控制器注解鉴权控制，基于redis做了储存优化，灵活支持单点/多点登陆

* 安装并配置了基于MySQL驱动的队列

* 注解生成接口文档，符合 OAS 3.0 的文档说明


#### 使用方法

```bash
# 1. 检查本地php环境，如果提示错误可能就是php拓展没开，打开后再试

	composer install

# 2. 启动开发运行，项目会启动到本地8000端口

	php think run

# 部署：使用nginx或apache映射 public 目录，设置伪静态指向index.php文件即可
```

## 特性说明

### 环境变量 env

支持多个环境变量文件的加载，具体参见 app/App.php 中的 loadEnv() 方法

### 登陆Token

提供了一个 /app/model/common/LoginCache.php 文件进行用户登录Token缓存的封装
使用trait这种非侵入式的方式，为任意模型类提供登录Token的缓存功能。
具体用法参见文件中的注释，或参考 /app/model/Admin.php 示例

本功能依赖think/Cache缓存类，并且必须使用Redis作为Cache的驱动。

储存token的方式是token和数据分开存，这样支持每个用户有多个token，且各自独立维护，又不会过多占用内存空间

pk:data
TOKEN1:pk
TOKEN2:pk

### 登陆鉴权

结合登陆TOKEN的功能，提供了一个中间件 app/middleware/AuthComment.php ，该中间件被控制器使用后，控制器可以直接使用注解的方式声明自己允许哪些身份的用户访问。具体逻辑参考该中间件的注释和 app/controller/adminapi/Admin.php

### 防爆破密码

在 app/extend/throtting 中提供了一种基于redis的漏斗算法限流方法，已用于 app/controller/adminapi/Admin.php 的 login 方法，具体参考文件。

如果无太强的安全需求，本限流方法用在登陆接口已经完全足够了。

现如今传统验证码抵抗爆破密码的能力已经微乎其微，真有安全方面需求还是接入第三方防火墙/中间件，避免在业务项目内部硬造轮子。

### API文档

在 /extend/openapi 中提供了一个自动生成API文档的工具
具体使用方法参见其中的README.md文件
本工具会自动扫描指定目录，并将其中符合规范的控制器类及其中控制器方法按照注解生成符合Swagger OAS 3.0规范的json文档，使用 yapi 之类标准API前端工具即可生成API文档页面

示例：
/app/controller/adminapi/Admin.php 定义Api接口
/app/controller/Index.php 生成Api文档

### 队列 Queue

使用ThinkPHP 6.0 官方的 topthink/think-queue 拓展类库实现队列
本框架使用了 MySQL 作为队列储存方式
由于官方文档（位于 /vendor/topthink/think-queue/README.md）对MySQL相关说明不详，本框架提供了一个示例和补充说明。
参见 /app/job/README.md