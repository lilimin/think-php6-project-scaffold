<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

Route::rule('index/:action', 'index/:action')->allowCrossDomain();

// 模块路由
// 未定义路由情况下，多级控制器访问方式是  {host}/{层级}.{控制器}/{方法} 例如 www.a.com/adminapi.user/login
// 只需要用变量将其转换为 {host}/{层级}/{控制器}/{方法} 即可
Route::rule(':module/:controller/:action', ':module.:controller/:action')->allowCrossDomain();
