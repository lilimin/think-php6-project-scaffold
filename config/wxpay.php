<?php
return [
	// 前面的appid什么的也得保留哦
	'app_id'             => '',
	'mch_id'             => '',
	'key'                => '',
	'cert_path'          => root_path() . '/cert/wxpay/apiclient_cert.pem', // XXX: 绝对路径！！！！
	'key_path'           => root_path() . '/cert/wxpay/apiclient_key.pem',      // XXX: 绝对路径！！！！
];
