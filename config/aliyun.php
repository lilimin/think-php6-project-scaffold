<?php

return [
	'accessKeyId' => '',
	'accessKeySecret' => '',

	'oss' => [
		// oss-cn-shanghai.aliyuncs.com
		'endpoint' => '',
		// gwxt-oss-main
		'bucket' => '',
	],

	'sts' => [
		// oss-cn-shanghai
		'regionId' => '',
	]
];
