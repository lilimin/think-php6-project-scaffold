<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------
use app\command\Example;
use coder\Command;

return [
	// 指令定义
	'commands' => [
		'example' => Example::class,
		// 根据表生成代码
		'coder' => Command::class,
	],
];
