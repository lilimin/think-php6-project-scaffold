<?php return array(
    'root' => array(
        'name' => 'topthink/think',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '46c7e633e08f415b80ead705bb2fdb78ac0b75c3',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'adbario/php-dot-notation' => array(
            'pretty_version' => '2.5.0',
            'version' => '2.5.0.0',
            'reference' => '081e2cca50c84bfeeea2e3ef9b2c8d206d80ccae',
            'type' => 'library',
            'install_path' => __DIR__ . '/../adbario/php-dot-notation',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'alibabacloud/aas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/actiontrail' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/adb' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/aegis' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/afs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/airec' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/alidns' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/alikafka' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/alimt' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/aliprobe' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/aliyuncvc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/appmallsservice' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/arms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/arms4finance' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/baas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/batchcompute' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/bss' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/bssopenapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/cas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/cbn' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ccc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ccs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/cdn' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/cds' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/cf' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/chatbot' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/client' => array(
            'pretty_version' => '1.5.32',
            'version' => '1.5.32.0',
            'reference' => '5bc6f6d660797dcee2c3aef29700ab41ee764f4d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alibabacloud/client',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'alibabacloud/cloudapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/cloudauth' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/cloudesl' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/cloudmarketing' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/cloudphoto' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/cloudwf' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/cms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/commondriver' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/companyreg' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/cr' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/crm' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/cs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/csb' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/cusanalyticsconline' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/dataworkspublic' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/dbs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/dcdn' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/dds' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/democenter' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/dm' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/dmsenterprise' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/domain' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/domainintl' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/drcloud' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/drds' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/dts' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/dybaseapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/dyplsapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/dypnsapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/dysmsapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/dyvmsapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/eci' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ecs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ecsinc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/edas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ehpc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/elasticsearch' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/emr' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ess' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/facebody' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/fnf' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/foas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ft' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/goodstech' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/gpdb' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/green' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/hbase' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/hiknoengine' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/hpc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/hsm' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/httpdns' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/idst' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/imageaudit' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/imageenhan' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/imagerecog' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/imagesearch' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/imageseg' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/imm' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/industrybrain' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/iot' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/iqa' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/itaas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ivision' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ivpd' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/jaq' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/jarvis' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/jarvispublic' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/kms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/linkedmall' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/linkface' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/linkwan' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/live' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/lubancloud' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/lubanruler' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/market' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/mopen' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/mpserverless' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/mts' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/multimediaai' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/nas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/netana' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/nlp' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/nlpautoml' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/nlscloudmeta' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/nlsfiletrans' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/objectdet' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ocr' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ocs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/oms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ons' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/onsmqtt' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/oos' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/openanalytics' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ossadmin' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ots' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/outboundbot' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/petadata' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/polardb' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/productcatalog' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/pts' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/push' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/pvtz' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/qualitycheck' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ram' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/rds' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/reid' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/retailcloud' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/rkvstore' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ros' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/rtc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/saf' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/sas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/sasapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/scdn' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/schedulerx2' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/sdk' => array(
            'pretty_version' => '1.8.1824',
            'version' => '1.8.1824.0',
            'reference' => '72a267bd25fba21f14d1eba82534f2351dc646d5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alibabacloud/sdk',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'alibabacloud/skyeye' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/slb' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/smartag' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/smc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/sms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/smsintl' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/snsuapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/sts' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/taginner' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/tesladam' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/teslamaxcompute' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/teslastream' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ubsms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/ubsmsinner' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/uis' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/unimkt' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/visionai' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/vod' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/voicenavigator' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/vpc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/vs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/wafopenapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/welfareinner' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/xspace' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/xtrace' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/yqbridge' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'alibabacloud/yundun' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1824',
            ),
        ),
        'aliyuncs/oss-sdk-php' => array(
            'pretty_version' => 'v2.6.0',
            'version' => '2.6.0.0',
            'reference' => '572d0f8e099e8630ae7139ed3fdedb926c7a760f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../aliyuncs/oss-sdk-php',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'clagiordano/weblibs-configmanager' => array(
            'pretty_version' => 'v1.2.0',
            'version' => '1.2.0.0',
            'reference' => '5c8ebcc62782313b1278afe802b120d18c07a059',
            'type' => 'library',
            'install_path' => __DIR__ . '/../clagiordano/weblibs-configmanager',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/guzzle' => array(
            'pretty_version' => '7.7.0',
            'version' => '7.7.0.0',
            'reference' => 'fb7566caccf22d74d1ab270de3551f72a58399f5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.5.3',
            'version' => '1.5.3.0',
            'reference' => '67ab6e18aaa14d753cc148911d273f6e6cb6721e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/psr7' => array(
            'pretty_version' => '2.5.0',
            'version' => '2.5.0.0',
            'reference' => 'b635f279edd83fc275f822a1188157ffea568ff6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/psr7',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'monolog/monolog' => array(
            'pretty_version' => '2.9.0',
            'version' => '2.9.0.0',
            'reference' => 'e1c0ae1528ce313a450e5e1ad782765c4a8dd3cb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../monolog/monolog',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'mtdowling/jmespath.php' => array(
            'pretty_version' => '2.6.1',
            'version' => '2.6.1.0',
            'reference' => '9b87907a81b87bc76d19a7fb2d61e61486ee9edb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../mtdowling/jmespath.php',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nesbot/carbon' => array(
            'pretty_version' => '2.66.0',
            'version' => '2.66.0.0',
            'reference' => '496712849902241f04902033b0441b269effe001',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nesbot/carbon',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nyholm/psr7' => array(
            'pretty_version' => '1.8.0',
            'version' => '1.8.0.0',
            'reference' => '3cb4d163b58589e47b35103e8e5e6a6a475b47be',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nyholm/psr7',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nyholm/psr7-server' => array(
            'pretty_version' => '1.0.2',
            'version' => '1.0.2.0',
            'reference' => 'b846a689844cef114e8079d8c80f0afd96745ae3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nyholm/psr7-server',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'overtrue/socialite' => array(
            'pretty_version' => '4.8.0',
            'version' => '4.8.0.0',
            'reference' => 'e55fdf50f8003be8f03a85a7e5a5b7c5716f4c9a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../overtrue/socialite',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-http/async-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
            ),
        ),
        'php-http/client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
            ),
        ),
        'php-http/message-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/cache' => array(
            'pretty_version' => '2.0.0',
            'version' => '2.0.0.0',
            'reference' => '213f9dbc5b9bfbc4f8db86d2838dc968752ce13b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/cache-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.2',
            'version' => '1.1.2.0',
            'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.2',
            'version' => '1.0.2.0',
            'reference' => 'e616d01114759c4c489f93b099585439f795fe35',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.1',
            'version' => '1.1.0.0',
            'reference' => 'cb6ce4845ce34a8ad9e68117c10ee90a29919eba',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/log-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0.0 || 2.0.0 || 3.0.0',
            ),
        ),
        'psr/simple-cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/simple-cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/simple-cache-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/cache' => array(
            'pretty_version' => 'v5.4.23',
            'version' => '5.4.23.0',
            'reference' => '983c79ff28612cdfd66d8e44e1a06e5afc87e107',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/cache-contracts' => array(
            'pretty_version' => 'v2.5.2',
            'version' => '2.5.2.0',
            'reference' => '64be4a7acb83b6f2bf6de9a02cee6dad41277ebc',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/cache-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/cache-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v3.0.2',
            'version' => '3.0.2.0',
            'reference' => '26954b3d62a6c5fd0ea8a2a00c0353a14978d05c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/http-client' => array(
            'pretty_version' => 'v6.0.20',
            'version' => '6.0.20.0',
            'reference' => '541c04560da1875f62c963c3aab6ea12a7314e11',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-client',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/http-client-contracts' => array(
            'pretty_version' => 'v3.0.2',
            'version' => '3.0.2.0',
            'reference' => '4184b9b63af1edaf35b6a7974c6f1f9f33294129',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-client-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '3.0',
            ),
        ),
        'symfony/http-foundation' => array(
            'pretty_version' => 'v6.0.20',
            'version' => '6.0.20.0',
            'reference' => 'e16b2676a4b3b1fa12378a20b29c364feda2a8d6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-foundation',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/mime' => array(
            'pretty_version' => 'v6.0.19',
            'version' => '6.0.19.0',
            'reference' => 'd7052547a0070cbeadd474e172b527a00d657301',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/mime',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-idn' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '639084e360537a19f9ee352433b84ce831f3d2da',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-idn',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '19bd1e4fcd5b91116f14d8533c57831ed00571b6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '8ad114f6b39e2c98a8b0e3bd907732c207c2b534',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '869329b1e9894268a8a61dabb69153029b7a8c97',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php73' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '9e8ecb5f92152187c4799efd3c96b78ccab18ff9',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php73',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '7a6ff3f1959bb01aefccb463a0f2cd3d3d2fd936',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php81' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '707403074c8ea6e2edaf8794b0157a0bfa52157a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php81',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/process' => array(
            'pretty_version' => 'v4.4.44',
            'version' => '4.4.44.0',
            'reference' => '5cee9cdc4f7805e2699d9fd66991a0e6df8252a2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/process',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/psr-http-message-bridge' => array(
            'pretty_version' => 'v2.2.0',
            'version' => '2.2.0.0',
            'reference' => '28a732c05bbad801304ad5a5c674cf2970508993',
            'type' => 'symfony-bridge',
            'install_path' => __DIR__ . '/../symfony/psr-http-message-bridge',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/service-contracts' => array(
            'pretty_version' => 'v2.5.2',
            'version' => '2.5.2.0',
            'reference' => '4b426aac47d6427cc1a1d0f7e2ac724627f5966c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/service-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/translation' => array(
            'pretty_version' => 'v6.0.19',
            'version' => '6.0.19.0',
            'reference' => '9c24b3fdbbe9fb2ef3a6afd8bbaadfd72dad681f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/translation-contracts' => array(
            'pretty_version' => 'v3.0.2',
            'version' => '3.0.2.0',
            'reference' => 'acbfbb274e730e5a0236f619b6168d9dedb3e282',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/translation-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '2.3|3.0',
            ),
        ),
        'symfony/var-dumper' => array(
            'pretty_version' => 'v4.4.47',
            'version' => '4.4.47.0',
            'reference' => '1069c7a3fca74578022fab6f81643248d02f8e63',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-dumper',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/var-exporter' => array(
            'pretty_version' => 'v6.0.19',
            'version' => '6.0.19.0',
            'reference' => 'df56f53818c2d5d9f683f4ad2e365ba73a3b69d2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-exporter',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'thenorthmemory/xml' => array(
            'pretty_version' => '1.1.1',
            'version' => '1.1.1.0',
            'reference' => '6f50c63450a0b098772423f8bdc3c4ad2c4c30bb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../thenorthmemory/xml',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/framework' => array(
            'pretty_version' => 'v6.1.3',
            'version' => '6.1.3.0',
            'reference' => '7c324e7011246f0064b055b62ab9c3921cf0a041',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/framework',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '46c7e633e08f415b80ead705bb2fdb78ac0b75c3',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-helper' => array(
            'pretty_version' => 'v3.1.6',
            'version' => '3.1.6.0',
            'reference' => '769acbe50a4274327162f9c68ec2e89a38eb2aff',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-helper',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-orm' => array(
            'pretty_version' => 'v3.0.9',
            'version' => '3.0.9.0',
            'reference' => 'af55970ee87833cf534d04c7e05284680daffb16',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-orm',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-queue' => array(
            'pretty_version' => 'v3.0.7',
            'version' => '3.0.7.0',
            'reference' => 'cded7616e313f9daa55c0ad0de5791f0d1fb3066',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-queue',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-trace' => array(
            'pretty_version' => 'v1.6',
            'version' => '1.6.0.0',
            'reference' => '136cd5d97e8bdb780e4b5c1637c588ed7ca3e142',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-trace',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'w7corp/easywechat' => array(
            'pretty_version' => '6.8.0',
            'version' => '6.8.0.0',
            'reference' => '60f0b4ba2ac3144df1a2291193daa34beb949d26',
            'type' => 'library',
            'install_path' => __DIR__ . '/../w7corp/easywechat',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
